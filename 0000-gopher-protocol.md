# The Gopher/2 Protocol

*Gopher/2 RFC 0*

| Author       | horsemans@horsemans.online |
| Status       | draft                      |
| Last updated | 2019-04-07                 |

In this RFC, we attempt to define a modern version of Gopher, with the
goal of creating an internet experience which is simple, easy to
understand and browse, painless to create clients and servers for, and
lightweight enough to use under even poor network conditions.

Gopher is an internet protocol which allows users to browse a
hierarchal set of pages and resources. Its initial specification,
[RFC1436][1], was written and released from 1991-1993, making the
protocol nearly 30 years old at the time of writing. The web,
developed concurrently, eventually became the *de facto* protocol for
most internet users since this time, and Gopher’s usage is a rounding
error of modern internet communication.

Gopher, and its simplicity, is being revisited in light of the
evolution of the modern web. Criticism of the web usually falls along
one of the following lines:

1. The web has largely become comprised of “walled gardens” such as
   Facebook, Pinterest, and other online presences. These presences,
   while usually free to use, operate by ingesting user data and
   selling aggregate information about this data to advertisers, which
   is seen as both a violation of privacy and an exploitation of user
   contributions to the web.

2. The web has become “bloated”, with web standards expanding to
   include CSS and JavaScript, web video, SVG, and more.

3. The web has become so complicated that web browsers can barely keep
   up, requiring enormous teams to maintain and support their network
   and rendering engines. HTTP itself has has to evolve significantly
   to support concurrent connections, streaming, and so on.

4. The complexity of web servers and browsers creates an enormous
   surface area for malicious actors to exploit. Google Chrome, for
   example, has over 80 CVEs assigned to it within the first three
   months of 2019.

5. Finally, the modern web has become a breeding ground for scams,
   phishing, malware, spyware, intrusive ads, and unnecessary use of
   resource-heavy multimedia such as auto-playing audio and video.

## Background

### Overview

The primary mode of displaying information in a Gopher response is a
set of strings separated by `\r\n`. Each line contains five fields:

1. A single character referring to the resource type that the line
   describes;
2. A human-readable string displayed to the user;
3. A *selector*, which tells the server where the user will navigate
   if the line described by the resource is selected;
4. The hostname which hosts the selector;
5. A port number.

Save for the first two items, which are not separated, all fields are
separated by `\t`.

## Design

The design of Gopher/2 follows.

Please note there are several specific goals and non-goals with
respect to the protocol definition. Specifically:

1. Requests and responses in Gopher/2 MUST be as simple to understand
   and manually construct as in Gopher.

2. Gopher/2 SHALL NOT attempt to accommodate usage other than simple
   transmission of site and resource data.

3. Gopher/2 MUST NOT introduce any out-of-band or side-channel
   metadata, such as request and response headers.

4. Gopher/2 SHALL NOT attempt to accommodate secure transmission and
   receipt and data; it is emphatically a protocol for insecure
   ingestion of site contents.

### Maps

A *map* (also *Gophermap*) is a list of selectors separated by `\n`.

No other content exists in a map response; where Gopher suggests a
full-stop once the request is complete, Gopher/2 servers should
instead terminate the connection after sending the list of selectors.

Maps must be represented as Unicode, encoded in UTF-8.

A selector is either a line of plain text, or a link to a resource
with a human-readable description. Links are prefixed with `@`, and
may be absolute or relative URLs. If there is a space after the URL,
it is consumed, and the remainder of the link is displayed as the
human-readable-link of the URL. If there is no content other than the
URL, it should be used directly as the human-readable link.

A sample map is demonstrated below:

```
Welcome to the Pineapple City Library Gopher!

Please visit one of our informational pages below.

@staff/ Meet Our Staff
@staff/president Meet Our Branch President

Enjoy this photograph of our new Physical Science Wing!

@images/physical_science_wing.png

@videos/groundbreaking.wmv?content=video/x-ms-wmv Watch our ground-breaking ceremony!

If you are looking for other libraries, please see below!

@gopher2://peachlibrary.site/ Peach Library Branch
@gopher2://strawberrylibrary.site:12334/ Strawberry Library Branch.
```

### URLs

There is no need to separate URL components into fields in modern
applications.

1. The scheme of Gopher/2 URLs is `gopher2`.

2. The default port of Gopher/2 URLs is 75, which is unused as of the
   time of writing.

3. It is an error to include a userinfo component in a Gopher/2 URL.

4. Gopher/2 URL query strings are key-value pairs associated with `=`
   and separated by the delimiter `&`. The query string key `content`
   is used to designate the URL’s content type, represented as a MIME
   type string. If a URL does not contain this key, the content type
   defaults to `text/gophermap`.

While passing a content query parameter to every URL may appear
cumbersome, it is a significantly easier approach than to attempt
side-channel metadata, such as in HTTP request/response headers.

Clients MAY choose to guess the content type of a URL based on its
filename. Servers MAY choose to elide the content type of a URL for
common media types.

#### Sample URLs

- `gopher2://example.com/`
- `gopher2://example.com/gopher.png?content=image/png`
- `gopher2://example.com/files/readme.md?content=text/markdown`

### Media Types

Gopher/2 leverages MIME types in order to provide more flexibility
than single character selector types.

We introduce a MIME type, `text/gophermap`, to assign to maps.

## Compatibility

Gopher/2 is *not* backwards-compatible with Gopher as defined by RFC
1436 and as used in practice, although it should be possible to
translate a single Gophermap line from Gopher to Gopher/2, and thereby
provide an automated conversion process from the former to the latter.

[1]: https://tools.ietf.org/rfc/rfc1436.txt
